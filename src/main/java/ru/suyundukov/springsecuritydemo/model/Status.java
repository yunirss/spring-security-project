package ru.suyundukov.springsecuritydemo.model;

public enum Status {
    ACTIVE,
    BANNED;
}
